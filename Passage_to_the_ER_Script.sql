-- Author : Marion Estoup
-- E-mail : marion_110@hotmail.fr
-- January 2021


-- Database: Passage_to_the_ER_DB

-- DROP DATABASE "Passage_to_the_ER_DB";

CREATE DATABASE "Passage_to_the_ER_DB"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Drop tables if they already exist before creating them
DROP TABLE IF EXISTS etapepassage;
DROP TABLE IF EXISTS passage; 
DROP TABLE IF EXISTS motif ; 
DROP TABLE IF EXISTS decisionSortie ; 
DROP TABLE IF EXISTS etape; 

-- Question 1 Outline of the data model corresponding to this context 
-- See the PDF provided


-- Question 2 Creation of the different tables 

-- Table for the different steps
CREATE TABLE etape(
	numEtape serial PRIMARY KEY,
	nomEtape varchar
);

-- Table for patient who can or can’t leave the hospital 
CREATE TABLE decisionSortie(
	numDecision serial PRIMARY KEY, 
	nomDecision varchar
);

-- Table for the reason that people come to the ER
CREATE TABLE motif(
	numMotif serial PRIMARY KEY, 
	nomMotif varchar 
);

-- Table for the passage to the ER
CREATE TABLE passage(
	numPassage serial PRIMARY KEY, 
	sexe varchar, 
	poids real, 
	taille real, 
	dateEntree date, 
	age integer,
	motif integer REFERENCES motif(numMotif) ON DELETE CASCADE, 
	decisionSortie integer REFERENCES decisionSortie(numDecision) ON DELETE CASCADE, 
	dateSortie date
);

-- Table for the different steps during a passage in the ER
CREATE TABLE etapepassage(
	etape integer REFERENCES etape(numEtape) ON DELETE CASCADE,
	passage integer REFERENCES passage(numPassage) ON DELETE CASCADE,
	ordre integer,
	PRIMARY KEY(etape, passage)
);



-- Question 3 Insertion of registration (at least 10 patients and various steps)

INSERT INTO etape(nomEtape) VALUES ('consultation medecin');
INSERT INTO etape(nomEtape) VALUES ('examen biologique');
INSERT INTO etape(nomEtape) VALUES ('soins infirmiers');
INSERT INTO etape(nomEtape) VALUES ('imagerie medicale');

INSERT INTO motif(nomMotif) VALUES ('brulure');
INSERT INTO motif(nomMotif) VALUES ('vomissements');
INSERT INTO motif(nomMotif) VALUES ('fievre');
INSERT INTO motif(nomMotif) VALUES ('maux de ventre');
INSERT INTO motif(nomMotif) VALUES ('maux de tete');

INSERT INTO decisionSortie(nomDecision) VALUES ('hospitalisation');
INSERT INTO decisionSortie(nomDecision) VALUES ('hospitalisation courte duree');
INSERT INTO decisionSortie(nomDecision) VALUES ('domicile');

-- Insertion of at least 10 patients
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (55, 'F', 45, 1.73, 1, 2);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (80, 'H', 60, 1.85, 2, 3);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (35, 'F', 55, 1.70, 3, 3);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (25, 'H', 60, 1.80, 4, 1);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (30, 'F', 55, 1.75, 5, 1);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (60, 'H', 50, 1.65, 3, 3);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (28, 'F', 60, 1.75, 2, 3);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (40, 'H', 58, 1.77, 1, 2);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (45, 'F', 65, 1.80, 5, 1);
INSERT INTO passage(age, sexe, poids, taille, motif, decisionSortie) VALUES (33, 'H', 56, 1.78, 4, 1);

INSERT INTO etapepassage VALUES (4, 4, 4);
INSERT INTO etapepassage VALUES (1, 2, 1);
INSERT INTO etapepassage VALUES (2, 2, 1);
INSERT INTO etapepassage VALUES (1, 4, 2);
INSERT INTO etapepassage VALUES (2, 4, 1);
INSERT INTO etapepassage VALUES (1, 3, 2);
INSERT INTO etapepassage VALUES (2, 3, 1);
INSERT INTO etapepassage VALUES (2, 1, 1);


-- Question 4 Export data base SQL and join the script to the exam


-- Question 4-1 Count the number of patients 
SELECT COUNT(*) nbPatients 
FROM passage;

-- Question 4-2 Count the number of patient by sex

SELECT sexe, COUNT(*)
FROM passage
GROUP BY sexe;

-- Question 4-3 Count the number of steps

SELECT COUNT(*) nbEtapes 
FROM etape;

-- Question 4-4 Count the mean, min, max of number of steps for all passages in the ER 

SELECT AVG(nbEtapesParPassage) moyenne, MIN(nbEtapesParPassage) MIN, MAX(nbEtapesParPassage) MAX
FROM(	
		SELECT COUNT(*) nbEtapesParPassage
		FROM etapepassage ep
		GROUP BY  ep.passage
	)
	AS T
;


-- Question 4-5 Count the number of steps for each patients 

SELECT *
FROM passage P, (
	SELECT ep.passage, COUNT(*) nbEtapesParPassage
	FROM etapepassage ep
	GROUP BY  ep.passage
	) AS T
WHERE P.numPassage = T.passage;



-- Question 4-6 Selection of patients that go home

SELECT *
FROM passage
WHERE numPassage IN (
	SELECT P.numPassage
	FROM passage P, decisionSortie d 
	WHERE d.nomDecision LIKE 'domicile'
	AND P.decisionSortie = d.numDecision
	)
;



-- Question 4-7 Selection of patients who had at least one biological examination

SELECT *
FROM passage
WHERE numPassage IN (
	SELECT DISTINCT ep.passage
	FROM etapepassage ep, etape e
	WHERE e.nomEtape LIKE 'examen de biologie'
	AND ep.etape = e.numEtape
	)
;


-- Question 4-8 Changement of the age of one patient

UPDATE passage
SET age = 23
WHERE numPassage = 1;



-- Question 4-9 Removal of one patient and his steps

DELETE FROM passage
WHERE numPassage = 2;



-- Question 4-10 Count the number of patients by destination 

SELECT d.nomDecision, COUNT(d.numDecision) nbPatients
FROM passage P, decisionSortie d
WHERE P.decisionSortie = d.numDecision
GROUP BY numDecision;


-- Question 4-11 Select patients who had at least one medical imaging but not a biological examination
SELECT *
FROM passage
WHERE numPassage IN (
	SELECT DISTINCT ep.passage
	FROM etapepassage ep, etape e
	WHERE e.nomEtape LIKE 'imagerie medicale' 
	AND e.nomEtape NOT LIKE 'examen biologique'
	)
;


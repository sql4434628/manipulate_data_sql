***Objective***

Create a Database to collect and store data related to a passage to the ER.
A passage to the ER is characterized by info about the patient (age, sex, weight, height, BMI, reason to come), the entrance and release dates from the ER.
The reason to come can be characterized by 20 categories here (fever, headache, etc).
The passage to the ER can be decomposed in several steps (nursing care, medical consult, biological exam, medical imaging, constants checkup).
The first step is always a constant checkup (blood pressure, heart rate) by the nurse.
In the last step the patient sees the doctor who tells him if he can leave the ER and go home, or hospitalization or short-term hospitalization.
Here we consider that one passage = one patient.

**Question 1** 

Outline of the data model corresponding to this context.

**Question 2** 

Creation of the different tables.

**Question 3** 

Insertion of registration (at least 10 patients and various steps).

**Question 4-1** 

Count the number of patients.

**Question 4-2** 

Count the number of patient by sex.

**Question 4-3** 

Count the number of steps.

**Question 4-4** 

Count the mean, min, max of number of steps for all passages in the ER.

**Question 4-5** 

Count the number of steps for each patients.

**Question 4-6** 

Selection of patients that go home.

**Question 4-7** 

Selection of patients who had at least one biological examination.

**Question 4-8** 

Change the age of one patient.

**Question 4-9** 

Removal of one patient and his steps.

**Question 4-10** 

Count the number of patients by destination.

**Question 4-11** 

Select patients who had at least one medical imaging but not a biological examination.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

January 2021
